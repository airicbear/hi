var month = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
var lastModified = new Date(document.lastModified);
document.getElementById("lastModified").innerHTML = "Last Modified: " + month[lastModified.getMonth()] + " " + lastModified.getDate() + ", " + (lastModified.getFullYear());